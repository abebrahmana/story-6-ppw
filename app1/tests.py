from django.test import TestCase, Client
from django.urls import resolve
from app1.views import index
from django.http import HttpRequest
from app1.models import Status
from app1.forms import StatusForm
from django.utils import timezone

# Create your tests here.
class Story6UnitTest(TestCase):
    def test_story6_url_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_story6_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_story6_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_story6_return_correct_html(self):
        request = HttpRequest()
        response = index(request)
        html = response.content.decode('utf8')
        self.assertTrue(html.startswith('<!DOCTYPE html>'))
        self.assertIn('<title>Story6</title>', html)
        self.assertTrue(html.endswith('</html>'))
        self.assertIn('Hello world', html)
        self.assertIn('Write your status', html)
        self.assertIn('This is your status', html)

    def test_story6_model_exist(self):
        Status.objects.create(name="jerome", content="Halo semua", date=timezone.now())
        countcontent = Status.objects.all().count()
        self.assertEqual(countcontent, 1)

    def can_save_a_post_request(self):
        response = self.client.post('/', data={'name':'joni', 'content':'bermain', 'date':'2020-03-03T19:14'})
        counting_all_available_activity = Status.objects.all().count()
        self.assertEqual(counting_all_available_activity, 1)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')
        new_response = self.client.get('/')
        html_response = new_response.content.decode('utf-8')
        self.assertIn('bermain', html_response)

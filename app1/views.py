from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Status
from .forms import StatusForm

# Create your views here.

def index(request):
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            data_item = form.save(commit=False)
            data_item.save()
        return redirect('/')
    else:
        status = StatusForm()
        data = Status.objects.all()
        response = {'Data':data, 'status': status}
        return render(request, 'index.html', response)
